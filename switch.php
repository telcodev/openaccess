<!DOCTYPE html>
<html>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>Welcome to OpenAccess WIFI!</title>

        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2751462220169204",
                enable_page_level_ads: true
            });
        </script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-66375359-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    </script>
    <style>

        body {
            background-color: #333333;
            margin: 0;
            padding: 0;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            width: 100vw;
            flex-direction: column;
        }

        input[type=submit] {
            color: #6DC90A;
            padding: 5px 15px 5px 15px;
            background: none;
            border: 2px solid #6DC90A;
            border-radius: 20px;
            margin-top: 10px;
        }

        p {
            font-size: 0.7em;
            font-weight: bold;
            margin: 0;
        }
    </style>
</head>
<body>

    <p>If nothing happens, please click the "CONTINUE" button below...</p>



    <?php
    $db_host = '41.191.232.10';
    $db_user = 'openaccess_user';
    $db_pass = 's3rv3r5mxdb';
    $db_name = 'vouchers';

    $link = mysqli_connect($db_host, $db_user, $db_pass, $db_name) or die('Failed to connect to the switch DB: ' . mysqli_connect_error());

    // the default openaccess landing page
    if (!isset($action))
        $action = 'http://41.191.232.10/openaccess_master/landing.php';

    echo '<form id="switch_form" method="post" action="' . $action . '">';
    foreach ($_REQUEST as $key => $val) {
        echo '<input type="hidden" name="' . $key . '" value="' . $val . '">';
    }
    echo '<input type="submit" class="btn btn:hover" value="CONTINUE>>>">'
    . '</form>';

    // close database link before we direct
    mysqli_close($link);
    ?>

                <script type="text/javascript">
                //document.getElementById("switch_form").submit();
                </script>
</body>
</html>

