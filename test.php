<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>OpenAccess Authorisation</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script>
        <?php include_once("./inc/analyticstracking.php"); ?>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2751462220169204",
                enable_page_level_ads: true
            });
        </script> 
    </head>

    <body class="body">

        <?php include './ui_partials/_header.php'; ?>

        <div class="main_body">
            <div class="form">

                <noscript>
                <a href='./advertising/www/delivery/ck.php?n=ae75f5aa&amp;cb=201603171138' target='_blank'>
                    <img src='./advertising/www/delivery/avw.php?zoneid=23&amp;cb=201603171138&amp;n=ae75f5aa' class="img-responsive" border='0' />
                </a>
                </noscript> 

                <p class="custom-welcome-text">Authorise</p>

                <label class="custom-label" for="auth-code-input">Enter Authorisation Code:</label>
                <input class="auth-code-input" type="text" name="verify_auth_code" size = "40" value="" id="auth-code-input" autofocus autocomplete="off">

                <p class="help-text green-text">Enter the authorisation code that was sent to you by SMS or Email </P>


                <p class="help-text">No code received? Resend by <a href="./resend_code.php">SMS</a> or <a href="./resend_code.php">Email</a></p>


                <button class="custom-button" type="button" id="submit-authorisation">Authorise</button>

                <p class="help-text">Or you can <a href="http://openaccess.co.zw/alogin.html"> return to log in</a></p>
            </div>
        </div>

        <?php include './ui_partials/_footer.php'; ?>

        <?php include './ui_partials/_help_button.php'; ?>

        <?php include './ui_partials/_notifier.php'; ?>

        <!-- Track outbound links in Google Analytics -->
        <script>
            (function ($) {

                "use strict";

                // current page host
                var baseURI = window.location.host;

                // click event on body
                $("body").on("click", function (e) {

                    // abandon if link already aborted or analytics is not available
                    if (e.isDefaultPrevented() || typeof ga !== "function")
                        return;

                    // abandon if no active link or link within domain
                    var link = $(e.target).closest("a");
                    if (link.length != 1 || baseURI == link[0].host)
                        return;

                    // cancel event and record outbound link
                    e.preventDefault();
                    var href = link[0].href;
                    ga('send', {
                        'hitType': 'event',
                        'eventCategory': 'outbound',
                        'eventAction': 'link',
                        'eventLabel': href,
                        'hitCallback': loadPage
                    });

                    // redirect after one second if recording takes too long
                    setTimeout(loadPage, 1000);

                    // redirect to outbound page
                    function loadPage() {
                        document.location = href;
                    }

                });

            })(jQuery); // pass another library here if required
        </script>

        <script type="text/javascript">

            $('#submit-authorisation').click(function () {

                    $('#submit-authorisation').text('Loading...');
                    $('#submit-authorisation').attr('disabled');
                    $.ajax({//create an ajax request to load_page.php
                        type: "POST",
                        url: "./ajax-scripts/test.php",
                        data: {auth_code: $('#auth-code-input').val()},
                        dataType: "json", //expect json to be returned

                        success: function (response) {
                            $('#submit-authorisation').text('Authorise');
                            $('#submit-authorisation').removeAttr('disabled');

                            ress = JSON.stringify(response);
                            res = JSON.parse(ress);
                            status = res['status'];
                            message = res['message'];
                            console.log(ress);

                            switch (status) {
                                case '1':
                                    showNotification('Congratulations', 'You have successfully authorised your OpenAccess account. We will redirect you to login in a few seconds.');
                                    $('#auth-code-input').val('');
                                    setTimeout(redir, 7500);
                                    function redir() {
                                        showNotification('Okay', message);
                                    }
                                    break;
                                case '0':
                                    showNotification('Sorry', message);
                                    break;
                                default:
                                    showNotification('Sorry', 'An unknown error occured');
                                    break;

                            }

                        },
                        error: function (jqXHR, exception) {
                            $('#submit-authorisation').text('Authorise');
                            $('#submit-authorisation').removeAttr('disabled');
                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to send authorise user');
                            console.log(jqXHR);
                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');


                        },
                        timeout: 10000

                    });

                
            });

        </script>

    </body>
</html>