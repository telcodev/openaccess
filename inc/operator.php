<?php

require("../inc/xmlrpc.inc");
require_once '../inc/ibsng_functions_j.php';

class OperatorClass {

    var $client;
    var $vouchers_connection;
    var $trial_users_connection;

    function __destruct() {
        mysqli_close($this->vouchers_connection);
        mysqli_close($this->trial_users_connection);
    }

    function OperatorClass() {

        date_default_timezone_set('Africa/Harare');

        $this->client = new IBSxmlrpcClient("172.31.254.186", "oa_admin", "0@adm1n");  // private ip for 41.191.236.37 (ibsng1)
        $db_username = "openaccess_user";
        $db_password = "s3rv3r5mxdb";
        $db_hostname = "localhost";

        $this->vouchers_connection = mysqli_connect($db_hostname, $db_username, $db_password, 'vouchers') or die("Unable to connect to vMySQL. Please contact OpenAccess Support on 086-8300-0010 " . mysqli_connect_error());
        $this->trial_users_connection = mysqli_connect($db_hostname, $db_username, $db_password, 'TrialUsers') or die("Unable to connect to tMySQL. Please contact OpenAccess Support on 086-8300-0010 " . mysqli_connect_error());
    }

    function topUpAccountUsingVoucher($mobile_num, $voucher_pin) {

        $user_id = $this->client->getUserInfoByCellPhone(strval($mobile_num));

        if ($user_id[0] != '1')
            return array("status" => 0, "message" => "An issue occured when we tried to identify your account with our billing server");

        if (!isset($user_id[1][0][0]))
            return array("status" => 0, "message" => "Mobile number could not be found");

        if ($user_id[1][0][0] == 0) 
            return array("status" => 0, "message" => "The mobile number you entered is not registered");

        $acct_bal = $this->client->getUserInfoByUserID(intval($user_id[1][0][0]));
        $bal = $acct_bal[1][$user_id[1][0][0]]['basic_info']['credit'];
        
        // when they top up if balance is less than zero first set their credit to zero
        if ($bal < 0){
            $res = $this->client->addToUserCreditEx((string) $user_id[1][0][0], 0, 'SET', 'Auto Clearing negative balance');
            if (!$res[0]) {
                    return array(
                        'status' => 0,
                        'message' => $res[1],
                        'forward-to-login' => false,
                    );
                }
        }

        $dump3 = $this->client->TopUpUserIDbyVoucher($user_id[1][0][0], $voucher_pin);

        if ($dump3[0] != '1')
            return array("status" => 0, "message" => $dump3[1]);

        $new_acct_bal = $this->client->getUserInfoByUserID(intval($user_id[1][0][0]));
        $new_bal = $new_acct_bal[1][$user_id[1][0][0]]['basic_info']['credit'];
        if ($new_bal >= 2 && $new_bal < 5) {
            $group_name = "Hotzone_250M";
            $this->client->changeUserGroupByUserId($user_id[1][0][0], $group_name);
        } else if ($new_bal >= 5 && $new_bal < 10) {
            $group_name = "Hotzone_1GB";
            $this->client->changeUserGroupByUserId($user_id[1][0][0], $group_name);
        } else if ($new_bal >= 10 && $new_bal < 15) {
            $group_name = "Hotzone_2GB";
            $this->client->changeUserGroupByUserId($user_id[1][0][0], $group_name);
        } else if ($new_bal >= 15) {
            $group_name = "Hotzone_5GB";
            $this->client->changeUserGroupByUserId($user_id[1][0][0], $group_name);
        } else if ($new_bal < 2) {
            $group_name = "Hotzone_110M";
            $this->client->changeUserGroupByUserId($user_id[1][0][0], $group_name);
        }
        return array("status" => 1, "message" => 'Topup was successful', 'group' => $group_name, 'balance' => $new_bal);
    }

    function updateUserVisits($mac, $locationid, $locationname) {
        $sql = "INSERT INTO `UserVisits`(`Username`,`VenueID`,`Venue`) VALUES ('$mac','$locationid','$locationname')";
        $result = mysqli_query($this->trial_users_connection, $sql);
        $row = mysqli_fetch_array($result);
        if ($result > 0) {
            return array("status" => 1, "message" => "User visit updated");
        }
        return array("status" => 0, "message" => "failed to update the user's visit");
    }

    function getUserInfo($searchBy, $value, $location = 'local') {
        switch ($location) {
            case 'local':

                $sql = "SELECT * FROM `TrialReg` WHERE `" . $searchBy . "` = '$value'";
                $result = mysqli_query($this->trial_users_connection, $sql);
                $row = mysqli_fetch_array($result);

                if ($result > 0) {
                    return array("status" => 1, "message" => $row);
                }
                return array("status" => 0, "message" => "No user found in Trial Registrations table");

                break;
            case 'ibs':

                break;
        }
    }

    function attemptAuthorisation($auth_code) {

        $sql = "SELECT * FROM `customer_registration` WHERE `RandomCode` = '$auth_code' AND `Confirmed` = 0";
        $result = mysqli_query($this->vouchers_connection, $sql);
        $record = mysqli_fetch_array($result);

        if (mysqli_num_rows($result) != 1) {
            return array("status" => 0, "code" => $auth_code, "message" => "Authorisation Code is incorrect or already registered. Enter the Correct Authorisation Code or contact OpenAccess Customer Support at 086-8300-0010");
        }

        $new_account = $this->client->addNewUser(1, 0.25, 'Main', 'OA-Accounts', $credit_comment = "Complimentary Signup Credit");

        if ($new_account[0] != true)
            return array("status" => 0, "message" => "Sorry, we were unable to create your temporary billing account", "for-tech" => $new_account);


        $name = $record{'Firstname'} . " " . $record{'Lastname'};
        $new_user_name = $record{'new_user_name'};
        $new_user_passwd = $record{'new_user_passwd'};
        $email = $record{'Email'};
        $cell_phone = $record{'Mobilenum'};


        ob_start();

        $this->client->setNormalUserAuth($new_account[1][0], $new_user_name, $new_user_passwd);
        $this->client->setVoipUserAuth($new_account[1][0], $new_user_name, $new_user_passwd);

        $this->client->setUserRealName($new_account[1][0], $name);
        $this->client->setUserEmail($new_account[1][0], $email);
        $this->client->setUserCellPhone($new_account[1][0], $cell_phone);

        $new_acct_bal = $this->client->getUserInfoByUserID(intval($new_account[1][0]));

        ob_end_clean();

        $sql2 = "UPDATE `customer_registration` SET `Confirmed`= 1 WHERE `RandomCode`= '$auth_code'";
        $result2 = mysqli_query($this->vouchers_connection, $sql2);

        $res = array(
            "status" => 1,
            "message" => "You have successfully completed the registration process!",
            "name" => $name,
            "user_id" => $new_account[1][0],
            "username" => $new_user_name,
            "password" => $new_user_passwd,
            "email" => $email,
            "mobile_number" => $cell_phone,
            "credit" => $new_acct_bal[1][$new_account[1][0]]['basic_info']['credit'],
            "email_result" => 'not-sent',
            "sms_result" => 'not-sent'
        );


        $subject = "New OpenAccess Account No: " . $new_account[1][0];
        $message = "<html><body><span style='color: #19CC0C'><h1>You have successfully completed the registration process!</h1></span><br>Congratulations" . $name . "<br>You are now a registered OpenAccess User!<br>Your account details are as follows: <br>User ID   :   " . $new_account[1][0] . "<br>Username  :   " . $new_user_name . "<br>Password  :   " . $new_user_passwd . "<br>Email     :   " . $email . "<br>Mobile No :   " . $cell_phone . "<br></body></html>";

        //send email
        $res['email_result'] = sendEmail($email, $subject, $message, $name);

        //send sms
        $txt = "OpenAccess Registration: Username: " . $new_user_name . " Password: " . $new_user_passwd . " For further support, please call 086-8300-0000";
        $res['sms_result'] = sendSms($cell_phone, $txt);

        return $res;
    }

    function resendAuthCode($val) {

        $val = preg_replace('/^00263|263|0|\+263/', '', $val);
        $val = '0' . $val;

        $sql2 = "SELECT * FROM `customer_registration` WHERE `Mobilenum` = '$val' AND `Confirmed` = 0";
        $sql3 = "SELECT * FROM `customer_registration` WHERE `Email` = '$val' AND `Confirmed` = 0";

        $result2 = mysqli_query($this->vouchers_connection, $sql2);
        $result3 = mysqli_query($this->vouchers_connection, $sql3);

        $row2 = mysqli_fetch_array($result2);
        $row3 = mysqli_fetch_array($result3);

        if ($row2 < 1 && $row3 < 1) {
            return array("status" => 0, "message" => "This mobile number / email is not registered or might have already been authorised. You can contact support on 08683 000 000", "for-tech" => $row3);
        }


        if ($row2 < 0) {
            $record = $row2;
        } else {
            $record = $row3;
        }

        $num = $record{'Mobilenum'};
        $email = $record{'Email'};


        $txt = "OpenAccess Registration: Confirm your account registration by entering authorisation code " . $auth_code . ". For support call 086-8300-0000";

        $to = $num;

        $res = array('status' => 1, 'message' => 'You have successfully registred');
        //send sms
        $res['sms_result'] = sendSms($to, $txt);
        //send email

        $res['email_result'] = sendEmail($email, 'OpenAccess Authorisation Code Request', $txt);

        return $res;
    }

    function attemptRegistration($remote_ip, $fname, $lname, $email, $mobilenum, $new_cust_username) {

        $sql = "SELECT RAS_Name from OA_AccessPoints where RAS_IP = '$remote_ip'";
        $ras_name_q = mysqli_query($this->vouchers_connection, $sql);
        $ras_name = mysqli_fetch_array($ras_name_q);

        $sql2 = "SELECT * FROM `customer_registration` WHERE `Mobilenum` = '$mobilenum'";
        $sql3 = "SELECT * FROM `customer_registration` WHERE `Email` = '$email'";

        $result2 = mysqli_query($this->vouchers_connection, $sql2);
        $result3 = mysqli_query($this->vouchers_connection, $sql3);

        $row2 = mysqli_num_rows($result2);
        $row3 = mysqli_num_rows($result3);

        //format the mobile number to the format we know
        $mobilenum = preg_replace('/^00263|263|^0|\+263/', '', $mobilenum);
        $mobilenum = '0' . $mobilenum;

        if (strlen($mobilenum) != 10)
            return array("status" => 0, "message" => "This number is invalid. Please enter a valid number in the format \"0771234567\" in order to continue registration!");

        $dump = $this->client->getUserIdByCellPhone($mobilenum);

        if ($row2 > 0 OR $dump == 'true')
            return array("status" => 0, "message" => "This mobile number number has already been registered");

        $dump2 = $this->client->getUserIdByEmail('$email');

        if ($row3 > 0 OR $dump2 == 'true')
            return array("status" => 0, "message" => "This email address has already been registered");

        $new_username_reg = $new_cust_username . substr(str_shuffle("0123456789"), 0, 3);
        $new_user_pass = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 6);
        $auth_code = rand(10000, 99999);

        $e164 = preg_replace('/^0+/', '263', $mobilenum);

        //insert the customer entered values into the database - new customer registrations table
        $timestamp = time();
        $sql = "INSERT INTO `customer_registration`(`Firstname`, `Lastname`, `Email`, `Mobilenum`, `RandomCode`,`new_user_name`, `new_user_passwd`, `Confirmed`, `AccessPoint_IP`,`RegistrationAP`, `RegistrationDate`) VALUES ('$fname','$lname','$email','$mobilenum','$auth_code','$new_username_reg','$new_user_pass','0', '$remote_ip', '$ras_name[0]', null)";
        $result = mysqli_query($this->vouchers_connection, $sql);

        if ($result != 1)
            return array("status" => 0, "message" => "There was an error capturing your detail. Verify that the information you entered is correct", "for-tech" => mysqli_error($this->vouchers_connection));


        $text = "OpenAccess Registration: Confirm your account registration by entering authorisation code " . $auth_code . ". For support call 086-8300-0000";

        $to = $e164;
        $transaction_code = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 6);


        $res = array('status' => 1, 'message' => 'You have successfully registered');
        //send sms
        $res['sms_result'] = sendSms($to, $text);
        return $res;
    }

    function registeredLogin($username) {

        $user_id = $this->client->getUserIdByNormalUsername($username);

        if (!is_int($user_id[1]) || $user_id[1] < 1) {
            return array(
                'status' => 0,
                'username' => $user_id[1],
                'message' => 'Your username seems to be invalid. Please verify that you typed the correct username',
                'forward-to-login' => false,
            );
        }


        $chk_info = $this->client->getUserInfoByNormalUserName($username);
        $current_bal = $chk_info[1][$user_id[1]]['basic_info']['credit'];
        $cell = $chk_info[1][$user_id[1]]['attrs']['cell_phone'];

        $sql2 = "SELECT * FROM `customer_registration` WHERE `Mobilenum` = '$cell' LIMIT 1";
        $result2 = mysqli_query($this->vouchers_connection, $sql2);
        $row2 = mysqli_fetch_assoc($result2);

        $last_login = $row2['last_login'];
        $last_login_plus_one_day = date('Y-m-d H:i:s', strtotime($last_login . ' +1 day'));
        $time_now = date('Y-m-d H:i:s');
        
        $credit_reset = false;

        // if credit for this user is under 0.25 they have less than 100MB we can either renew it for them if it has been 24hours or inform them they cannot browse anymore today
        if ($current_bal < 0.25) {

            // it has been 24 hours, we can renew it for them
            if (strtotime($time_now) > strtotime($last_login_plus_one_day)) {
                $res = $this->client->addToUserCreditEx((string) $user_id[1], 0.25, 'SET', 'Auto 24 Hour daily renewal');
                if (!$res[0]) {
                    return array(
                        'status' => 0,
                        'message' => $res[1],
                        'forward-to-login' => false,
                    );
                }
                $credit_reset = true;
            } else {
                // it has not yet been 24 hours, just check if their credit is under zero and kick them out
                if ($current_bal <= 0) {
                    return array(
                        'status' => 0,
                        'message' => 'You have exahausted your free quota for today. You can either top up, or come back again after 24 hours for another free browsing session.',
                        'forward-to-login' => false,
                    );
                }
            }
        }

        //all processing done, proceed to login and set the last login date
        $sql3 = "UPDATE `customer_registration` SET last_login = '$time_now' WHERE `Mobilenum` = '$cell'";
        mysqli_query($this->vouchers_connection, $sql3);
        return array(
            'status' => 1,
            'message' => 'Proceed to login...',
            'forward-to-login' => true,
            'credit-reset' => $credit_reset
        );
    }

    function createTrialAccount($remote_ip, $mac) {

        $max_free_logins = 3;

//Search whether the MAC address has ever been registered before creating a new account
        $sql2 = "SELECT * FROM `TrialReg` WHERE `Mac_Address` = '$mac' LIMIT 1";
        $result2 = mysqli_query($this->trial_users_connection, $sql2);
        $row2 = mysqli_fetch_array($result2);

        if ($row2 == 0) {

            date_default_timezone_set('Africa/Harare');
            $created_date = date('Y-m-d H:i:s', time());
            $characters = array("-", " ", ":");
            $trimmed_date = str_replace($characters, "", $created_date);
            $trial_username = 'Trial' . $trimmed_date;

            $sql3 = "SELECT * FROM `TrialReg` WHERE `Trial_Username` = '$trial_username'";
            $result3 = mysqli_query($this->trial_users_connection, $sql3);
            $row3 = mysqli_fetch_array($result3);
            if ($row3 > 0) {
                $trimmed_date2 = $trimmed_date + 1;
                $trial_username = 'Trial' . $trimmed_date2;
            }
//generate a random password
            $trial_password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 10);

//set the quota for the account in kbps
            $trial_quota = 51200;

//Capture the RAS IP so that we can identify which RAS the user is registering from
            $ras = $remote_ip;

//Once we have checked that the account does not exist, create a new temporary user account
            $sql = "INSERT INTO `TrialReg`(`TrialUserID`,`Mac_Address`, `Trial_Username`, `Trial_Password`, `Trial_Quota`, `Created_Date`, `Last_Login`, `RAS_ID`) VALUES ('', '$mac', '$trial_username', '$trial_password', '$trial_quota', '$created_date', null, '$ras')";
            $result = mysqli_query($this->trial_users_connection, $sql);

            if ($result !== TRUE)
                return array("status" => 0, "message" => "Sorry, we were unable to store your temporary account information");

            //Add a new trial user in the IBSng
            $new_account = $this->client->addNewUser(1, 0.10, 'Main', 'OA-TrialUsers', $credit_comment = "Trial User StartUp Credit");

            if ($new_account[0] != true)
                return array("status" => 0, "message" => "Sorry, we were unable to create your temporary billing account");

            ob_start();
            $internet_ac = $this->client->setNormalUserAuth($new_account[1][0], $trial_username, $trial_password);
            $new_acct_bal = $this->client->getUserInfoByUserID(intval($new_account[1][0]));
            ob_end_clean();

            return array(
                'status' => 1,
                'message' => 'Congratulations! You are now a registered OpenAccess Trial User',
                'user_id' => $new_account[1][0],
                'username' => $trial_username,
                'password' => $trial_password,
                'credit' => $new_acct_bal[1][$new_account[1][0]]['basic_info']['credit'],
                'forward-to-login' => true,
            );  // to do on this return automatically forward user details to log in
        } else {
//If the MAC address is already in the database
            $trial_username = $row2[2];
            $trial_password = $row2[3];
            $user_id = $this->client->getUserIdByNormalUsername($trial_username);

            $chk_info = $this->client->getUserInfoByNormalUserName($trial_username);
            $current_bal = $chk_info[1][$user_id[1]]['basic_info']['credit'];

            $last_login = $row2[6];
            $last_login_plus_one_day = date('Y-m-d H:i:s', strtotime($last_login . ' +1 day'));
            $time_now = date('Y-m-d H:i:s');

            // if credit for this user is 0 we can either renew it for them if it has been 24hours or inform them they cannot browse anymore today
            if ($current_bal < 0.1) {

                // this guy has exhausted and it has not been 24hours yet
                if (strtotime($time_now) < strtotime($last_login_plus_one_day)) {
                    $diff = strtotime($last_login_plus_one_day) - strtotime($time_now);
                    $date_diff = date('h:i', $diff);
                    return array(
                        'status' => 0,
                        'message' => 'You have exhausted your free data available for today. Please wait for ' . $date_diff . ' minutes before you trying to connect again.',
                        'forward-to-login' => false,
                    );
                } else {
                    // it has been 24 hours, we can renew it for them
                    $res = $this->client->renewUsers((string) $user_id[1], '24 Hour daily renewal');

                    if (!$res[0]) {
                        return array(
                            'status' => 0,
                            'message' => $res[1],
                            'forward-to-login' => false,
                        );
                    }
                }
            }

            //let us update the free_logins
            $sql2 = "SELECT `free_logins` FROM `TrialReg` WHERE `Mac_Address` = '$mac'";
            $result2 = mysqli_query($this->trial_users_connection, $sql2);
            $free_logins = mysqli_fetch_assoc($result2)['free_logins'];
            $remaining_logins = $max_free_logins - $free_logins;

            if ($free_logins >= $max_free_logins) {
                return array(
                    'status' => 0,
                    'message' => 'You have no more free session avaliable. Please register or activate your account or log in using your username and password',
                    'forward-to-login' => false,
                    'remaining-logins' => $remaining_logins
                );
            }

            $time_now = date('Y-m-d H:i:s');
            $sql3 = "UPDATE `TrialReg` SET free_logins = free_logins + 1, Last_Login = '$time_now' WHERE `Mac_Address` = '$mac'";
            $result3 = mysqli_query($this->trial_users_connection, $sql3);

            return array(
                'status' => 0,
                'message' => 'Your device has enrolled with OpenAccess before. We will automatically log you in, in a few seconds. Please note: You have ' . --$remaining_logins . ' free sessions remaining.',
                'user_id' => $user_id[1][0],
                'username' => $trial_username,
                'password' => $trial_password,
                'forward-to-login' => true,
                'remaining-logins' => --$remaining_logins
            );
            // forward this already registered guy to login
        }
    }

    function recoverAccount($recoveryid) {

        //if this is a number lets format it to a way we know
        if (preg_match('/^[0-9]{9,15}/', $recoveryid)) {
            $recoveryid = preg_replace('/^00263|263|0|\+263/', '', $recoveryid);
            $recoveryid = '0' . $recoveryid;
        }

        $user_id = $this->client->getUserInfoByCellPhone($recoveryid);
        if (!empty($user_id[1])) {
            $recover_userid = $user_id[1][0][0];
        } else {
            $user_id = $this->client->getUserInfoByEmail($recoveryid);
            $recover_userid = $user_id[1][0][0];
        }

        $user_info = $this->client->getUserInfoByUserID($recover_userid);

        $recovered_username = $user_info[1][$recover_userid]['attrs']['normal_username'];
        $recovered_passwd = $user_info[1][$recover_userid]['attrs']['normal_password'];
        $recovered_mobile = $user_info[1][$recover_userid]['attrs']['cell_phone'];
        $recovered_email = $user_info[1][$recover_userid]['attrs']['email'];

        if (empty($recovered_username) OR empty($recovered_passwd))
            return array('status' => 0, 'message' => 'This account is not registered');

        if ($recovered_username == 'r')
            return array('status' => 0, 'message' => 'There is no valid registered account with the details you supplied.');

        $res = array(
            'status' => 1,
            'message' => 'You account details have been recovered',
            'username' => $recovered_username,
            'password' => $recovered_passwd,
            'mobile' => $recovered_mobile,
            'email' => $recovered_email,
            'sms_status' => 'not-sent',
            'email_status' => 'not-sent',
        );

//TXT Messaging Script
        $text = "OpenAccess Recovery: Username: " . $recovered_username . " Password: " . $recovered_passwd . " For further support, please call 086-8300-0000";

        $res['sms_status'] = sendSms($recovered_mobile, $text);
        $res['email_status'] = sendEmail($recovered_email, 'OpenAccess Account Recovery', $text);
        return $res;
    }

    function changePassword($cell, $old_password, $new_password) {

        $cell = preg_replace('/^00263|263|0|\+263/', '', $cell);
        $cell = '0' . $cell;

        $dump4 = $this->client->getUserInfoByCellPhone($cell);

        if ($dump4[0] != 'true')
            return array('status' => 0, 'message' => 'This mobile number is not registered. Please register first before trying to manage your account', 'for-tech' => $dump4);

        $recover_userid = $dump4[1][0][0];

        if (empty($recover_userid))
            return array('status' => 0, 'message' => 'This email address has no account information');

        $user_info = $this->client->getUserInfoByUserID($recover_userid);

        $recovered_fullname = $user_info[1][$recover_userid]['attrs']['name'];
        $recovered_username = $user_info[1][$recover_userid]['attrs']['normal_username'];
        $recovered_passwd = $user_info[1][$recover_userid]['attrs']['normal_password'];

        if ($recovered_passwd !== $old_password)
            return array('status' => 0, 'message' => 'The old password you supplied appears to be incorrect. Please go to account recovery to retrieve your old password');

        ob_start();
        $setpasswd = $this->client->setPasswordOfNormalUser($recovered_username, $new_password);
        ob_end_clean();


        if ($setpasswd[0] != 1)
            return array('status' => 0, 'message' => 'Password reset failed, please again try later');

        $res = array(
            'status' => 1,
            'message' => 'Your password was successfully changed.',
            'email_status' => 'not-sent',
        );

        $emailSubject = "OpenAccess Hotspot Password Reset: " . $recover_userid;
        $emailBody = "<html><body><span style='color: #19CC0C'><h1>Your OpenAccess WiFi Account Password has been changed!</h1></span><br>OpenAccess Password Change: Your WiFi password has been changed / reset.<br> Should you not have requested this password recovery, please contact the Systems Administrator immediately. <br>For further support, please call 086-8300-0000<br></body></html>";

        $res['email_status'] = sendEmail($recovered_email, $emailSubject, $emailBody);

        $sql2 = "UPDATE `customer_registration` SET `new_user_passwd` = '$new_password' WHERE `Mobilenum`= '$cell'";
        $res['db-update'] = mysqli_query($this->vouchers_connection, $sql2);

        return $res;
    }

}

function sendSms($recipients, $message) {

    $body = urlencode($message);
    $baseurl = "http://www.txt.co.zw";
    $username = 'telcoremote';
    $full_txt_url = $baseurl . "/Remote/SendMessage?Username=" . $username . "&Recipients=" . $recipients . "&Body=" . $body;
    $res = get_web_page($full_txt_url);

    $exploded = explode(':', $res);

    if ($exploded[0] == 'SUCCESS') {
        return 'sent';
    }
    return $exploded[1];
}

function sendEmail($recipients, $subject, $message, $name = '') {

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
    $headers .= 'From: <noreply@telco.co.zw>' . "\r\n";
    $headers .= 'Cc: jayden@telco.co.zw' . "\r\n";

    $message = wordwrap($message, 100);
    return mail($recipients, $subject, $message, $headers);
}

function clean_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function get_web_page($url) {
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        CURLOPT_ENCODING => "", // handle compressed
        CURLOPT_USERAGENT => "TelcoAPI v1.0", // name of client
        CURLOPT_AUTOREFERER => true, // set referrer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // time-out on connect
        CURLOPT_TIMEOUT => 120, // time-out on response
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);

    $content = curl_exec($ch);

    curl_close($ch);

    return $content;
}
