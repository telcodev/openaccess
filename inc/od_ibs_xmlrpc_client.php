<?php


class IBSxmlrpcClient {
    /*

      All request methods return an array containing exit status (TRUE or FALSE) and result

      if exit status was FALSE means that the request was failed and the second item if error message, for example:
      array(FALSE, "Access Denied")

      if exit status was TRUE, means that the request was successfull and
      the second item is an array containing result of that request, for example:
      array(TRUE, array(1, 2, 3))

     */

    function IBSxmlrpcClient($server_ip, $auth_name, $auth_pass, $auth_type = "ADMIN", $server_port = "1235", $timeout = 1800) {
        $this->client = new xmlrpc_client("/", $server_ip, $server_port);

        $this->auth_params = array(
            "auth_name" => $auth_name,
            "auth_pass" => $auth_pass,
            "auth_type" => $auth_type,
        );

        $this->client->setDebug(FALSE);
        $this->timeout = $timeout;
    }

            
    function sendRequest($server_method, $params) {
        /*
          Send request to $server_method, with parameters $params
          $server_method: method to call ex admin.addNewAdmin
          $params: an array of parameters
         */

        $xml_val = php_xmlrpc_encode($params + $this->auth_params);
        $xml_msg = new xmlrpcmsg($server_method);
        $xml_msg->addParam($xml_val);

        $response = $this->client->send($xml_msg, $this->timeout);

        if ($response == FALSE)
            $result = array(FALSE, "Error occured while connecting to server");
        else if ($response->faultCode() != 0)
            $result = array(FALSE, $response->faultString());
        else
            $result = array(TRUE, php_xmlrpc_decode($response->value()));

        unset($response);
        return $result;
    }

    function getUserInfoByUserID($user_id) {
        return $this->sendRequest("user.getUserInfo", array("user_id" => (string) $user_id));
    }

    function getUserInfoByNormalUserName($normal_username) {
        return $this->sendRequest("user.getUserInfo", array("normal_username" => $normal_username));
    }

    function getUserInfoByViopUserName($voip_username) {
        return $this->sendRequest("user.getUserInfo", array("voip_username" => $voip_username));
    }

    function getUserInfoByCellPhone($cell_phone) {
        return $this->sendRequest("user.getUsersWithCellPhone", array("cell_phone" => $cell_phone));
    }

    function getUserInfoByEmail($email) {
        return $this->sendRequest("user.getUsersWithEmail", array("email" => $email));
    }

    function getUserInfoBySerial($serial) {
        return $this->sendRequest("user.getUserInfo", array("serial" => $serial));
    }

    function addNewUser($count, $credit, $isp_name, $group_name, $credit_comment = "") {
        return $this->sendRequest("user.addNewUsers", array(
                    "count" => $count,
                    "credit" => $credit,
                    "isp_name" => $isp_name,
                    "group_name" => $group_name,
                    "credit_comment" => $credit_comment
        ));
    }

    //function to add a user using a voucher PIN
    function addNewUserByVoucher($voucher_pin, $isp_name, $group_name) {
        return $this->sendRequest("voucher.voucherAddNewUser", array(
                    "voucher_pin" => $voucher_pin,
                    "isp_name" => $isp_name,
                    "group_name" => $group_name));
    }

    function setUserAttribute($user_id, $attr_name, $attr_value) {
        return $this->sendRequest("user.updateUserAttrs", array(
                    "user_id" => (string) $user_id,
                    "attrs" => array($attr_name => $attr_value),
                    "to_del_attrs" => array()
        ));
    }

    function TopUpUserIDbyVoucher($user_id, $voucher_pin) {
        return $this->sendRequest("voucher.voucherRechargeUser", array(
                    "user_id" => (int) $user_id,
                    "voucher_pin" => (string) $voucher_pin
        ));
    }

    function setUserAttributes($user_id, $attrs) {
        var_dump($attrs);
        return $this->sendRequest("user.updateUserAttrs", array(
                    "user_id" => (string) $user_id,
                    "attrs" => $attrs,
                    "to_del_attrs" => array()
        ));
    }

    function deleteUserAttribute($user_id, $attr_name) {
        return $this->sendRequest("user.updateUserAttrs", array(
                    "user_id" => (string) $user_id,
                    "attrs" => array(),
                    "to_del_attrs" => array($attr_name)
        ));
    }

    function deleteUserAttributes($user_id, $attrs) {
        return $this->sendRequest("user.updateUserAttrs", array(
                    "user_id" => (string) $user_id,
                    "attrs" => array(),
                    "to_del_attrs" => $attrs
        ));
    }

    function setNormalUserAuth($user_id, $username, $password) {
        return $this->setUserAttributes(
                        $user_id, array(
                    "normal_user_spec" => array(
                        "normal_username" => $username,
                        "normal_password" => $password
                    )
                        )
        );
    }

    function setPasswordOfNormalUser($username, $password) {
        $user_id_res = $this->getUserIdByNormalUsername($username);
        if (!$user_id_res[0]) {
            return $user_id_res;
        }
        $user_id = $user_id_res[1];
        return $this->setNormalUserAuth($user_id, $username, $password);
    }

    function setVoipUserAuth($user_id, $username, $password) {
        return $this->setUserAttributes($user_id, array("voip_user_spec" => array("voip_username" => $username, "voip_password" => $password)));
    }

    function setUserComment($user_id, $comment) {
        return $this->setUserAttribute($user_id, "comment", $comment);
    }

    function setUserRealName($user_id, $name) {
        return $this->setUserAttribute($user_id, "name", $name);
    }

    function setUserEmail($user_id, $email) {
        return $this->setUserAttribute($user_id, "email", $email);
    }

    function setUserPhone($user_id, $phone) {
        return $this->setUserAttribute($user_id, "phone", $phone);
    }

    function setUserCellPhone($user_id, $cell_phone) {
        return $this->setUserAttribute($user_id, "cell_phone", $cell_phone);
    }

    function setUserBirthDate($user_id, $birth_date, $date_unit = "gregorian") {
        return $this->setUserAttributes($user_id, array("birthdate" => $birth_date, "birthdate_unit" => $date_unit));
    }

    function setUserAddress($user_id, $address) {
        return $this->setUserAttribute($user_id, "address", $address);
    }

    function setUserPostalCode($user_id, $postal_code) {
        return $this->setUserAttribute($user_id, "postal_code", $postal_code);
    }

    function lockUser($user_id, $lock_reason = "") {
        return $this->setUserAttributes($user_id, array("lock" => $lock_reason));
    }

    function unlockUser($user_id) {
        return $this->deleteUserAttributes($user_id, array("lock"));
    }

    function setRelExpDate($user_id, $rel_exp_date, $date_unit = "Days") {
        return $this->setUserAttributes($user_id, array("rel_exp_date" => $rel_exp_date, "rel_exp_date_unit" => $date_unit));
    }

    function deleteRelExpDate($user_id) {
        return $this->deleteUserAttributes($user_id, array("rel_exp_date"));
    }

    function setAbsExpDate($user_id, $abs_exp_date, $date_unit = "gregorian") {
        return $this->setUserAttributes($user_id, array("abs_exp_date" => $abs_exp_date, "abs_exp_date_unit" => $date_unit));
    }

    function deleteAbsExpDate($user_id) {
        return $this->deleteUserAttributes($user_id, array("abs_exp_date"));
    }

    function searchUser($conds) {
        $result = $this->sendRequest("user.searchUser", array(
            "conds" => $conds,
            "from" => 1,
            "to" => 50000,
            "order_by" => "user_id",
            "desc" => FALSE
        ));
        if (!$result[0]) {
            return $result;
        }
        return array(TRUE, $result[1][2]);
    }

    function searchNeverLoggedInUsers() {## date filter on what????
        return $this->searchUser(array("never_logged_in_users" => TRUE));
    }

    function searchUserRelExpDateTo($to_date, $date_unit = "gregorian") {
        return $this->searchUser(array(
                    "rel_exp_date_to" => $to_date,
                    "rel_exp_value_unit" => $date_unit
        ));
    }

    function getOnlineUsersWithRemoteIP() {## FIXME remote_ip
        $result = $this->sendRequest("report.getOnlineUsers", array(
            "conds" => array(),
            "normal_sort_by" => "user_id",
            "normal_desc" => FALSE,
            "voip_sort_by" => "user_id",
            "voip_desc" => FALSE,
        ));
        $succeed = $result[0];
        if (!$succeed) {
            return $result;
        }
        $internet_onlines = $result[1][0];
        #$voip_onlines = $result[1][0];
        $internet_onlines_abs = array();
        foreach ($internet_onlines as $user_info) {
            $internet_onlines_abs[$user_info["user_id"]] = $user_info["attrs"]["remote_ip"];
        }
        return array(TRUE, $internet_onlines_abs);
    }

    function getConnectionLogs($login_time_from, $login_time_to, $user_ids = null, $remote_ip = null, $date_unit = "gregorian") {
        $conds = array(
            "login_time_from" => $login_time_from,
            "login_time_from_date_unit" => $date_unit,
            "login_time_to" => $login_time_to,
            "login_time_to_date_unit" => $date_unit,
            "show_total_duration" => "On"
        );

        if ($user_ids) {
            $conds["user_ids"] = $user_ids;
        }

        if ($remote_ip) {
            $conds["remote_ip"] = $remote_ip;
        }

        $result = $this->sendRequest("report.getConnections", array(
            "conds" => $conds,
            "from" => 1,
            "to" => 50000,
            "sort_by" => "user_id", ## FIXME
            "desc" => FALSE
        ));

        if (!$result[0]) {
            return $result;
        }
        return $result;
    }

    function getConnectionLogsForMac($login_time_from, $login_time_to, $mac, $date_unit = "gregorian") {
        $conds = array(
            "login_time_from" => $login_time_from,
            "login_time_from_date_unit" => $date_unit,
            "login_time_to" => $login_time_to,
            "login_time_to_date_unit" => $date_unit,
            "mac" => $mac
        );


        $result = $this->sendRequest("report.getConnections", array(
            "conds" => $conds,
            "from" => 1,
            "to" => 50000,
            "sort_by" => "login_time", ## FIXME
            "desc" => TRUE
        ));

        if (!$result[0]) {
            return $result;
        }

        $report = $result[1]["report"];
        $report_abs = array();

        foreach ($report as $item) {
            $mac = NULL;
            foreach ($item["details"] as $det) {
                if ($det[0] == "mac") {
                    $mac = $det[1];
                    break;
                }
            }
            $report_abs[] = array(
                "user_id" => $item["user_id"],
                "login_time" => $item["login_time_formatted"],
                "logout_time" => $item["logout_time_formatted"],
                "bytes_in" => $item["bytes_in"],
                "bytes_out" => $item["bytes_out"],
                "remote_ip" => $item["remote_ip"],
                "mac" => $mac
            );
        }

        return array(TRUE, $report_abs);
    }

    function getConnectionLogsTrafficMoreThan($more_than_bytes, $login_time_from, $login_time_to, $date_unit = "gregorian") {
        $result = $this->sendRequest("report.getConnections", array(
            "conds" => array(
                "login_time_from" => $login_time_from,
                "login_time_from_date_unit" => $date_unit,
                "login_time_to" => $login_time_to,
                "login_time_to_date_unit" => $date_unit,
            ),
            "from" => 1,
            "to" => 50000,
            "sort_by" => "user_id", ## FIXME
            "desc" => FALSE
        ));

        if (!$result[0]) {
            return $result;
        }

        $report = $result[1]["report"];
        $report_abs = array();

        foreach ($report as $item) {
            $bytes = $item["bytes_in"] + $item["bytes_out"];
            if ($bytes > $more_than_bytes) {
                $report_abs[] = array(
                    "user_id" => $item["user_id"],
                    "bytes" => $bytes,
                    "bytes_in" => $item["bytes_in"],
                    "bytes_out" => $item["bytes_out"],
                    "login_time" => $item["login_time_formatted"],
                    "logout_time" => $item["logout_time_formatted"],
                );
            }
        }
        return array(TRUE, $report_abs);
    }

    function killUser($user_id) {
        return $this->sendRequest("user.killUserByID", array(
                    "user_id" => (string) $user_id
        ));
    }

    function setStaticIp($user_id, $ip) {
        return $this->setUserAttributes("assign_ip", $user_id, $ip);
    }

    function unsetStaticIp($user_id) {
        return $this->deleteUserAttribute("assign_ip", $user_id);
    }

    function searchUserByCreationDate($creation_date_from, $creation_date_to, $date_unit = "gregorian") {
        return $this->searchUser(array(
                    "creation_date_from" => $creation_date_from,
                    "creation_date_from_unit" => $date_unit,
                    "creation_date_to" => $creation_date_to,
                    "creation_date_to_unit" => $date_unit
        ));
    }

    function searchUserByCredit($credit_op, $credit) {
        return $this->searchUser(array(
                    "credit_op" => $credit_op,
                    "credit" => $credit
        ));
    }

    function searchUserByCellPhone($cell_phone) {
        return $this->searchUser(array(
                    "cell_phone" => $cell_phone
        ));
    }

    function setUserCredit($user_id, $credit, $comment = "") {
        return $this->sendRequest("user.changeCredit", array(
                    "user_id" => $user_id,
                    "credit" => $credit,
                    "is_absolute_change" => FALSE,
                    "credit_comment" => $comment
        ));
    }

    function addToUserCredit($user_id, $credit, $comment = "") {
        return $this->sendRequest("user.changeCredit", array(
                    "user_id" => $user_id,
                    "credit" => $credit,
                    "is_absolute_change" => TRUE,
                    "credit_comment" => $comment,
                    "is_renew" => "1"
        ));
    }
    
    function setUserDeposit($user_id, $deposit, $comment = "") {
        return $this->sendRequest("user.changeDeposit", array(
                    "user_id" => $user_id,
                    "deposit" => $deposit,
                    "is_absolute_change" => FALSE,
                    "deposit_comment" => $comment
        ));
    }

    function addToUserDeposit($user_id, $deposit, $comment = "") {
        return $this->sendRequest("user.changeDeposit", array(
                    "user_id" => $user_id,
                    "deposit" => $deposit,
                    "is_absolute_change" => TRUE,
                    "deposit_comment" => $comment
        ));
    }

    function getUserIdByNormalUsername($username) {
        $info_result = $this->getUserInfoByNormalUserName($username);
        if ($info_result[0]) {
            $keys = array_keys($info_result[1]);
            if (count($keys) == 0) {
                return array(FALSE, "No user with internet username '$username'");
            } else {
                return array(TRUE, $keys[0]);
            }
        } else {
            return $info_result;
        }
    }

    function getUserIdByEmail($email) {
        $info_result = $this->getUserInfoByEmail($email);
        if ($info_result[0]) {
            $keys = array_keys($info_result[1]);
            if (count($keys) == 0) {
                return array(FALSE, "No user with email found!");
            } else {
                $r_uid = array(TRUE, $info_result[1][0][0]);
                $var_uid = $r_uid[0];
                return $var_uid;
            }
        } else {
            return $info_result;
        }
    }

    function getUserIdByCellPhone($cell_phone) {
        $info_result = $this->getUserInfoByCellPhone($cell_phone);
        if ($info_result[0]) {
            $keys = array_keys($info_result[1]);
            if (count($keys) == 0) {
                return array(FALSE, "No user with cell phone found");
            } else {
                $r_uid = array(TRUE, $info_result[1][0][0]);
                $var_uid = $r_uid[0];
                return $var_uid;
            }
        } else {
            return $info_result;
        }
    }

    function getCreditChangesByUsername($username, $change_time_from, $change_time_to, $date_unit = "gregorian") {
        $user_id_res = $this->getUserIdByNormalUsername($username);
        if (!$user_id_res[0]) {
            return $user_id_res;
        }
        $user_id = $user_id_res[1];
        //var_dump($user_id);
        $result = $this->sendRequest("report.getCreditChanges", array(
            "conds" => array(
                "user_ids" => "$user_id",
                "change_time_from" => $change_time_from,
                "change_time_from_unit" => $date_unit,
                "change_time_to" => $change_time_to,
                "change_time_to_unit" => $date_unit
            ),
            "from" => 1,
            "to" => 50000,
            "sort_by" => "change_time", ## FIXME
            "desc" => FALSE
        ));

        return $result;

        /*
          if (!$result[0]){
          return $result;
          }

          $report = $result[1]["report"];

          $report_abs = array();

          foreach($report as $item){
          $item_uids = $item["user_ids"];
          foreach($item_uids as $i => $item_uid){
          if($item_uid == $user_id){
          $report_abs[] = $item;
          break;
          }
          }
          }
          return array(TRUE, $report_abs);
         */
    }

    function changeUserGroupByUserId($user_id, $group_name) {
        $this->setUserAttribute($user_id, "group_name", $group_name);
    }

    function changeUserGroupByUsername($username, $group_name) {
        $user_id_res = $this->getUserIdByNormalUsername($username);
        if (!$user_id_res[0]) {
            return $user_id_res;
        }
        $user_id = $user_id_res[1];
        $this->setUserAttribute($user_id, "group_name", $group_name);
    }

    function getCreditChanges() {
        return $this->sendRequest("report.getCreditChanges", array("conds" => array("user_view" => "1"), "from" => 1, "to" => 2, "sort_by" => "change_time", "desc" => False));
    }

    function searchExpiredUsers($exp_date_from, $exp_date_to, $date_unit = "gregorian") {
        $result = $this->sendRequest("user.searchExpiredUsers", array(
            "conds" => array(
                "exp_date_from" => $exp_date_from,
                "exp_date_from_from_unit" => $date_unit,
                "exp_date_to" => $exp_date_to,
                "exp_date_to_unit" => $date_unit
            ),
            "from" => 1,
            "to" => 50,
            "order_by" => "user_id", ## FIXME
            "desc" => FALSE
        ));

        return $result;
    }

//new function added
    function renewUsers($user_ids, $comment) {
        return $this->sendRequest("user.renewUsers", array("user_id" => $user_ids, "comment" => $comment));
    }

    function getUserDepositChanges() {
        return $this->sendRequest("report.getUserDepositChanges", array("conds" => array("user_id" => "1"), "from" => 1, "to" => 20, "sort_by" => "change_time", "desc" => False));
    }

    function getISPDepositChangeLogs() {
        return $this->sendRequest("report.getISPDepositChangeLogs", array("conds" => array("isp_id" => "1"), "from" => 1, "to" => 2, "sort_by" => "isp_deposit_change_admin_id", "desc" => False));
    }

    function getISPDeposit($isp_name) {
        return $this->sendRequest("isp.getISPInfo", array("isp_name" => $isp_name));
    }

}

?>

