<div class="notification-box">
    <div class='notification-body'>
        <div class='notification-header'>
            <span id="notify-header">Alert</span>
        </div>
        <div class='notification-content'>
            <span id="notify-message">
                Welcome to OpenAccess.
            </span>
            <button class='notify-button'>Dismiss</button>
        </div>
    </div>
</div>

<script type="text/javascript">    
    function showNotification(title, message) {
       $('#notify-header').text(title);
       $('#notify-message').html(message);
       $(".notification-box").slideDown(300);
       $('button').attr('disabled', 'disabled');
       $('button').removeAttr('disabled');
       $('.notify-button').removeAttr('disabled');
    }
    $('.notify-button').click(function () {
        $('.notification-box').slideUp(300);
        $('button').removeAttr('disabled');
    });
</script>