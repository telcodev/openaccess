<button class="help-button">?</button>
<script type="text/javascript">
    var msg = `
        <ul>
            <li> A new stunning, dark gray and lime green theme user interface</li>
            <li> A new side menu to put all your common actions on your finger tips</li>
            <li> Brand new interactive notification panel</li>
            <li> Faster and more responsive actions all around the interface</li>
            <li> Animated buttons and controls</li>
            <li> Background processing of all requests for a more responsive UI.</li>
        </ul>

    `;
    $('.help-button').click(function(){
        showNotification('What\'s new in OpenAccess', msg);
    });
</script>