<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$recoveryid = clean_input($_POST['recoveryid']);

$response = $operator->recoverAccount($recoveryid);

echo json_encode($response);

