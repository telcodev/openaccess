<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$mobile = clean_input($_POST['mobile']);
$voucher_pin = clean_input($_POST['voucher_pin']);

$response = $operator->topUpAccountUsingVoucher($mobile, $voucher_pin);

echo json_encode($response);

