<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$remote_ip = clean_input($_POST['remote_ip']);
$fname = clean_input($_POST['firstname']);
$lname = clean_input($_POST['surname']);
$email = clean_input($_POST['email']);
$mobile_num = clean_input($_POST['mobile_num']);
$username = clean_input($_POST['username']);

$response = $operator->attemptRegistration($remote_ip, $fname, $lname, $email, $mobile_num, $username);

echo json_encode($response);

