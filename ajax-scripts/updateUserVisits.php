<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$mac = clean_input($_POST['mac']);
$locationid = clean_input($_POST['locationid']);
$locationname = clean_input($_POST['locationname']);

$response = $operator->updateUserVisits($mac, $locationid, $locationname);

echo json_encode($response);

