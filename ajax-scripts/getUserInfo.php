<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

require '../inc/operator.php';

$operator = new OperatorClass();

$searchBy = clean_input($_POST['search_by']);
$value = clean_input($_POST['value']);

$response = $operator->getUserInfo($searchBy, $value);

echo json_encode($response);

