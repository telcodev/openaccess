<?php
$mac = $_POST['mac'];
$ip = $_POST['ip'];
$username = $_POST['username'];
$link_login = $_POST['link-login'];
$link_orig = $_POST['link-orig'];
$error = $_POST['error'];
$chap_id = $_POST['chap-id'];
$chap_challenge = $_POST['chap-challenge'];
$link_login_orig = $_POST['link-login-orig'];
$link_orig_esc = $_POST['link-orig-esc'];
$location_id = $_POST['location-id'];
$location_name = $_POST['location-name'];
$mac_esc = $_POST['mac-esc'];
$interface_name = $_POST['interface-name'];
?>

<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>Welcome to OpenAccess Hotspot</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script>
        <?php include_once("./inc/analyticstracking.php"); ?>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2751462220169204",
                enable_page_level_ads: true
            });
        </script> 
    </head>

    <body class="body">
        

        <?php include './ui_partials/_header.php'; ?>
        
        <?php include './ui_partials/_sidebar.php'; ?>

        <div class="main_body">

            <div class="form">

                <h1 class="custom-welcome-text">Sorry</h1>

                <h3 class="help-text error-intro-text">Looks like you were unable to sign on to the HotSpot because:</h3>

                <h3 class="error">

                    <?php
                    if (stristr($error, 'credit')) {
                        echo 'Your data quota has been exhausted, You will have to wait for a while before you can connect again.';
                    } else if (stristr($error, 'not registered')) {
                        echo 'This device is not registered. Please complete the registration steps and authorise your account in order to use OpenAccess.';
                    } else {
                        echo $error;
                    }
                    ?>

                </h3>

                <div class="buttons-container">

                    <a class="custom-button" href="<?php echo $link_login; ?>"> Try Again? </a>

                    <a class="custom-button" href="mailto:jayden@telco.co.zw?cc=tawanda@telco.co.zw&subject=OpenAcess Hotspot Portal Issue" target="_blank"> Report a Problem</a>

                </div>

            </div>
            <!-- /.container -->

            <?php include './ui_partials/_help_button.php'; ?>

        </div>

        <?php include './ui_partials/_footer.php'; ?>

    </body>
</html>
