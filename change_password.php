<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>OpenAccess Password Change</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script>
        <?php include_once("./inc/analyticstracking.php"); ?>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2751462220169204",
                enable_page_level_ads: true
            });
        </script> 
    </head>

    <body class="body">

        <?php include './ui_partials/_header.php'; ?>
        
        <?php include './ui_partials/_sidebar.php'; ?>

        <div class="main_body remove-flex">



            <div class="form">

                <noscript>
                <a href='./advertising/www/delivery/ck.php?n=ae75f5aa&amp;cb=201603171138' target='_blank'>
                    <img src='./advertising/www/delivery/avw.php?zoneid=23&amp;cb=201603171138&amp;n=ae75f5aa' class="img-responsive" border='0' />
                </a>
                </noscript> 

                <p class="custom-welcome-text">Reset</p>

                <label class="custom-label" for="cell">Mobile Number:</label>
                <input placeholder="eg: 0771234567" class="auth-code-input" type="text" name="cell" size = "80" value="" id="cell" autofocus autocomplete="off">
                <p class="help-text green-text">Enter the email address that you used to register </P>

                <label class="custom-label" for="old_pass">Current Password:</label>
                <input class="auth-code-input" type="password" name="old_pass" size = "80" value="" id="old_pass" autofocus autocomplete="off">
                <p class="help-text green-text">Supply the your current password </P>

                <label class="custom-label" for="new_pass">New Password:</label>
                <input class="auth-code-input" type="password" name="new_pass" size = "80" value="" id="new_pass" autofocus autocomplete="off">
                <p class="help-text green-text">Supply your new password. It should be 6 characters or more. </P>

                <label class="custom-label" for="new_pass2">Repeat New Password:</label>
                <input class="auth-code-input" type="password" name="new_pass2" size = "80" value="" id="new_pass2" autofocus autocomplete="off">
                <p class="help-text green-text">Repeat your new password </P>


                <p class="help-text">Forgot your password? Go to <a href="./recover_details.php">password recovery</a></p>


                <button class="custom-button" type="button" id="submit-reset">Change Password</button>

                <p class="help-text">Or you can <a href="http://openaccess.co.zw/alogin.html"> proceed to log in</a></p>
            </div>
        </div>

        <?php include './ui_partials/_footer.php'; ?>

        <?php include './ui_partials/_help_button.php'; ?>

        <?php include './ui_partials/_notifier.php'; ?>

        <!-- Track outbound links in Google Analytics -->
        <script>
            (function ($) {

                "use strict";

                // current page host
                var baseURI = window.location.host;

                // click event on body
                $("body").on("click", function (e) {

                    // abandon if link already aborted or analytics is not available
                    if (e.isDefaultPrevented() || typeof ga !== "function")
                        return;

                    // abandon if no active link or link within domain
                    var link = $(e.target).closest("a");
                    if (link.length != 1 || baseURI == link[0].host)
                        return;

                    // cancel event and record outbound link
                    e.preventDefault();
                    var href = link[0].href;
                    ga('send', {
                        'hitType': 'event',
                        'eventCategory': 'outbound',
                        'eventAction': 'link',
                        'eventLabel': href,
                        'hitCallback': loadPage
                    });

                    // redirect after one second if recording takes too long
                    setTimeout(loadPage, 1000);

                    // redirect to outbound page
                    function loadPage() {
                        document.location = href;
                    }

                });

            })(jQuery); // pass another library here if required
        </script>

        <script type="text/javascript">

            $('#submit-reset').click(function () {

                var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if ($('#cell').val() < 10) {
                    showNotification('Sorry', 'Please make sure you have supplied a valid mobile number.');
                } else if ($('#new_pass').val().toString().length < 6) {
                    showNotification('Sorry', 'Please make sure your password is at least 6 chracters long.');
                } else if ($('#new_pass').val() !== $('#new_pass2').val()) {
                    showNotification('Sorry', 'Please make sure your 2 passwords are matching');
                } else {
                    $('#submit-reset').text('Loading...');
                    $('#submit-reset').attr('disabled');
                    $.ajax({//create an ajax request to load_page.php
                        type: "POST",
                        url: "./ajax-scripts/changePassword.php",
                        data: {cell: $('#cell').val(), old_pass: $('#old_pass').val(), new_pass: $('#new_pass').val()},
                        dataType: "json", //expect json to be returned

                        success: function (response) {
                            $('#submit-reset').text('Change Password');
                            $('#submit-reset').removeAttr('disabled');

                            ress = JSON.stringify(response);
                            res = JSON.parse(ress);
                            status = res['status'];
                            message = res['message'];
                            console.log(ress);

                            switch (status) {
                                case '1':
                                    showNotification('Congratulations', 'Your password has been changed and you can proceed to log in with your new credentials.');
                                    break;
                                case '0':
                                    showNotification('Sorry', message);
                                    break;
                                default:
                                    showNotification('Sorry', 'An unknown error occured');
                                    break;

                            }

                        },
                        error: function (jqXHR, exception) {
                            $('#submit-reset').text('Change Password');
                            $('#submit-reset').removeAttr('disabled');
                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to change password');
                            console.log(jqXHR);
                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                        },
                        timeout: 10000

                    });
                }

            });

        </script>

    </body>
</html>