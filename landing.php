
<?php
//Receive POST variables from the Mikrotik Router
$mac = $_POST['mac'];
$ip = $_POST['ip'];
$username = $_POST['username'];
$linklogin = $_POST['link-login'];
$linkorig = $_POST['link-orig'];
$error = $_POST['error'];
$chapid = $_POST['chap-id'];
$chapchallenge = $_POST['chap-challenge'];
$linkloginonly = $_POST['link-login-only'];
$linkorigesc = $_POST['link-orig-esc'];
$macesc = $_POST['mac-esc'];
$locationid = $_POST['location-id'];
$locationname = $_POST['location-name'];

if (isset($_POST['mac'])) {
    $mac = $_POST['mac'];
} else {

    echo '<h2>Hotspot initialisation error!</h2><br>';
    echo '<h4>There was an error when we tried to collect details about your device. For security reasons, you will have to restart the log in process</h4> <br>';
    echo '<h5>This page will redirect you in a short while</h5>';
    header('refresh:10; url=http://openaccess.co.zw/alogin.html');
    echo '<p>If the page does not redirect you automatically, <a href="http://openaccess.co.zw/alogin.html">click here</a> </p>';

    exit();
}

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $remote_ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $remote_ip = $_SERVER['REMOTE_ADDR'];
}
?>

<head>

    <meta charset="utf-8"> 
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
    <title>Welcome to OpenAccess</title>
    <link rel="icon" type="image/png" href="./img/favicon.ico">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/styles.css">
    <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script> 
    <?php include_once("./inc/analyticstracking.php"); ?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2751462220169204",
            enable_page_level_ads: true
        });
    </script> 



</head>

<body class="body">


    <?php include './ui_partials/_header.php'; ?>
    
    <?php include './ui_partials/_sidebar.php'; ?>
    
    <?php include './ui_partials/_authorise_button.php'; ?> 

    <div class="landing-main_body">


        <div class="landing-form">

            <noscript>
            <a href='./advertising/www/delivery/ck.php?n=ae75f5aa&amp;cb=201603171138' target='_blank'>
                <img src='./advertising/www/delivery/avw.php?zoneid=23&amp;cb=201603171138&amp;n=ae75f5aa' class="img-responsive" border='0' />
            </a>
            </noscript> 

            <!--Tabbed Menu-->

            <ul class="tabs-list">
                <li><a href="#tab2success" data-toggle="tab">Login Here</a></li>
                <li class="active"><a href="#tab1success" data-toggle="tab">Try FREE!</a></li>
                <li><a href="#tab3success" data-toggle="tab">TopUp Here</a></li>
                <li><a href="#tab4success" data-toggle="tab">Recharge Account</a></li>
                <li><a href="#tab5success" data-toggle="tab">Register New User</a></li>
            </ul>

            <div class="tabs-body-container">
                <div class="tab-content"> 

                    <!--Tab 1: Guest User-->
                    <div class="tab-pane fade in active" id="tab1success">
                        <p class="help-text">Brought to you by our Sponsor:</p>

                        <iframe id='a443290e' name='a443290e' src='./advertising/www/delivery/afr.php?zoneid=21&amp;cb=201603171222' frameborder='0' scrolling='no' width='250' height='332'>
                            <a href='./advertising/www/delivery/ck.php?n=ac4484a0&amp;cb=201603171223' target='_blank'>
                                <img src='./advertising/www/delivery/avw.php?zoneid=21&amp;cb=201603171223&amp;n=ac4484a0' class="img-responsive" border='0' alt='' />
                            </a>
                        </iframe>


                        <button type="submit" class="custom-button" id="browse-free-submit-button">GET 100MB FREE!</button>

                        <p class="help-text">Valid for <span class="green-text">24 Hours</span></p>

                        <p class="help-text">By using this service you are agreeing to our Standard Terms and Conditions regarding our sponsored advertising messages and our fair usage policy.</p>
                        <script type="text/javascript">
                            $('#browse-free-submit-button').click(function () {

                                var regex = new RegExp("^(?:[[:xdigit:]]{2}([-:]))(?:[[:xdigit:]]{2}\1){4}[[:xdigit:]]{2}$");
                                if (regex.test('<?php echo $mac; ?>')) {

                                    showNotification('Sorry', 'We failed to collect your device details. We will redirect you in a short while and try again.');
                                    setTimeout(redir, 7500);
                                    function redir() {
                                        window.location = '<?php echo $linklogin; ?>';
                                    }


                                } else {
                                    $('#browse-free-submit-button').text('Loading...');
                                    $('#browse-free-submit-button').attr('disabled');
                                    $.ajax({//create an ajax request to getUserInfo.php when doc loads
                                        type: "POST",
                                        url: "./ajax-scripts/createTrialAccount.php",
                                        data: {remote_ip: '<?php echo $remote_ip; ?>', mac: '<?php echo $mac; ?>'},
                                        dataType: "json", //expect json to be returned  

                                        success: function (response) {
                                            $('#browse-free-submit-button').text('GET 100MB FREE!');
                                            $('#browse-free-submit-button').removeAttr('disabled');
                                            ress = JSON.stringify(response);
                                            res = JSON.parse(ress);
                                            status = res['status'];
                                            message = res['message'];
                                            console.log(ress);
                                            switch (status) {
                                                case '1':
                                                    showNotification('Congratulations', 'You have 100MB worth of free data. You can now begin browsing. We will redirect you in a few seconds.');
                                                    if (res['forward-to-login']) {
                                                        $('#username').val(res['username']);
                                                        $('#password').val(hexMD5('<?php echo $chapid; ?>' + res["password"] + '<?php echo $chapchallenge; ?>'));
                                                        setTimeout(redir, 7500);
                                                        function redir() {
                                                            $('#registered-user-login-form').submit();
                                                        }
                                                    }
                                                    break;
                                                case '0':
                                                    showNotification('Sorry', message);
                                                    if (res['forward-to-login']) {
                                                        $('#username').val(res['username']);
                                                        $('#password').val(hexMD5('<?php echo $chapid; ?>' + res["password"] + '<?php echo $chapchallenge; ?>'));
                                                        setTimeout(redir, 5000);
                                                        function redir() {
                                                            $('#registered-user-login-form').submit();
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    showNotification('Sorry', 'An error occured: ' + message);
                                                    break;
                                            }

                                        },
                                        error: function (jqXHR, exception) {
                                            $('#browse-free-submit-button').text('GET 100MB FREE!');
                                            $('#browse-free-submit-button').removeAttr('disabled');
                                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to create new trial account');
                                            console.log(jqXHR);
                                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                                        },
                                        timeout: 10000
                                    });
                                }

                            });
                        </script>
                    </div>

                    <!--Tab 1: Guest User Finished-->

                    <!--Tab 2: Registered User-->
                    <div class="tab-pane fade" id="tab2success">

                        <form class="login-form" name="login" role="form" action="./login.php" method="post" id="registered-user-login-form">
                            <input type="hidden" name="mac" id="mac" value="<?php echo $mac; ?>" />
                            <input type="hidden" name="ip" id="ip" value="<?php echo $ip; ?>" />
                            <input type="hidden" name="link-login" id="link-login" value="<?php echo $linklogin; ?>" />
                            <input type="hidden" name="link-orig" id="link-orig" value="<?php echo $linkorig; ?>" />
                            <input type="hidden" name="error" id="error" value="<?php echo $error; ?>" />
                            <input type="hidden" name="chap-id" id="chap-id" value="<?php echo $chapid; ?>" />
                            <input type="hidden" name="chap-challenge" id="chap-challenge" value="<?php echo $chapchallenge; ?>" />
                            <input type="hidden" name="link-login-only" id="link-login-only" value="<?php echo $linkloginonly; ?>" />
                            <input type="hidden" name="link-orig-esc" id="link-orig-esc" value="<?php echo $linkorigesc; ?>" />
                            <input type="hidden" name="mac-esc" id="mac-esc" value="<?php echo $macesc; ?>" />
                            <input type="hidden" name="remote_ip" id="remote_ip" value="<?php echo $remote_ip; ?>" />



                            <label class="custom-label" for="username">Username / Email:</label>
                            <input type="text" class="auth-code-input" name="username" id="username"  id="username" autocomplete="off">



                            <label class="custom-label" for="password">Password:</label><br>
                            <input type="password" name="password" id="password" class="auth-code-input" id="password">


                            <button class="custom-button" type="button" id="registered-user-login-button">LOGIN</button>

                            <p class="help-text">Lost your username / password? <a href="./recover_details.php">Recover them here</a>, or <a href="./change_password.php">Reset your password here</a></p>
                        </form>
                        <script type="text/javascript" src="./js/md5.js"></script>
                        <script type="text/javascript">

                            $('#registered-user-login-button').click(function () {

                                if ($('#username').val().toString().length < 2 || $('#password').val().length < 2) {
                                    showNotification('Sorry', 'Please make sure you have filled in both your username and password');
                                } else {
                                    $('#registered-user-login-button').text('Logging in...');
                                    $('#registered-user-login-button').attr('disabled');
                                    $.ajax({//create an ajax request to registerdLogin.php when doc loads
                                        type: "POST",
                                        url: "./ajax-scripts/registeredLogin.php",
                                        data: {
                                            username: $('#username').val().toString()
                                        },
                                        dataType: "json", //expect json to be returned  

                                        success: function (response) {
                                            $(this).text('LOGIN');
                                            $(this).removeAttr('disabled');
                                            ress = JSON.stringify(response);
                                            res = JSON.parse(ress);
                                            status = res['status'];
                                            message = res['message'];
                                            console.log(ress);
                                            switch (status) {
                                                case '1':

                                                    if (res['credit-reset']) {
                                                        showNotification('Congratulations', 'We have awarded you with a fresh new allocation of data worth over 100MB. We will log you in, in a few seconds');
                                                        setTimeout(redir, 10000);
                                                        function redir() {
                                                            // ricochet to login
                                                            $('#password').val(hexMD5('<?php echo $chapid; ?>' + $('#password').val() + '<?php echo $chapchallenge; ?>'));
                                                            $('#registered-user-login-form').submit();
                                                        }
                                                    } else {
                                                        // ricochet to login
                                                        $('#password').val(hexMD5('<?php echo $chapid; ?>' + $('#password').val() + '<?php echo $chapchallenge; ?>'));
                                                        $('#registered-user-login-form').submit();
                                                    }


                                                    break;
                                                case '0':
                                                    showNotification('Sorry', message);
                                                    $('#registered-user-login-button').text('LOGIN');
                                                    $('#registered-user-login-button').removeAttr('disabled');
                                                    break;
                                                default:
                                                    showNotification('Sorry', 'An error occured: ' + message);
                                                    break;
                                            }

                                        },
                                        error: function (jqXHR, exception) {
                                            $('#registered-user-login-button').text('LOGIN');
                                            $('#registered-user-login-button').removeAttr('disabled');
                                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to login a registered member');
                                            console.log(jqXHR);
                                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                                        },
                                        timeout: 10000
                                    });
                                }

                            });
                        </script>

                    </div>

                    <!--Tab 1: Registered User Finished-->

                    <!--Tab 3: Voucher User-->
                    <div class="tab-pane fade" id="tab3success">

                        <a href="https://www.topup.co.zw/buy-openaccess-wifi-vouchers-online" target="_blank">
                            <img alt="TopUp Now" src="./img/topup_openaccess.jpg" class="topup-img">
                        </a>
                        <br>
                        <button class="custom-button" onclick="window.location.href = 'https://www.topup.co.zw/buy-openaccess-wifi-vouchers-online'" target="_blank">Buy Now!</button>
                        <br>
                        <p class="help-text">
                            You can Top Up your OpenAccess account 24 hours a day, 7 days a week using <span class="green-text">Ecocash</span>, <span class="green-text">Telecash</span>, <span class="green-text">VPayments</span>, <span class="green-text">Mastercard</span> or <span class="green-text">VISA</span>!
                        </p>


                    </div>

                    <!--Tab 3: Voucher User Finished-->

                    <!--Tab 4: Top UP User / Buy Voucher -->
                    <div class="tab-pane fade" id="tab4success">

                        <form class="login-form"  role="form" action="" method="POST" enctype="multipart/form-data">

                            <label for="mobile_num" class="custom-label">Enter Mobile Number:</label><br>
                            <input class="auth-code-input" type="text" id="mobile_num">
                            <p class="help-text green-text">Enter the mobile number linked to your account</p>

                            <label for="voucher_pin" class="custom-label">Enter Voucher PIN:</label><br>
                            <input type="text" class="auth-code-input" id="voucher_pin">
                            <p class="help-text green-text">Enter the Voucher PIN and click Submit</p>

                            <button class="custom-button" type="button" id="top-up-submit-button" >Submit</button>

                        </form>
                        <script type="text/javascript">
                            $('#top-up-submit-button').click(function () {

                                if ($('#mobile_num').val().toString().length < 10 || $('#voucher_pin').val().length < 3) {
                                    showNotification('Sorry', 'Please make sure your mobile number and voucher pin is correct');
                                } else {
                                    $('#top-up-submit-button').text('Loading...');
                                    $('#top-up-submit-button').attr('disabled');
                                    $.ajax({//create an ajax request to getUserInfo.php when doc loads
                                        type: "POST",
                                        url: "./ajax-scripts/topUpAccountUsingVoucher.php",
                                        data: {mobile: $('#mobile_num').val(), voucher_pin: $('#voucher_pin').val()},
                                        dataType: "json", //expect json to be returned  

                                        success: function (response) {
                                            $('#top-up-submit-button').text('Submit');
                                            $('#top-up-submit-button').removeAttr('disabled');
                                            ress = JSON.stringify(response);
                                            res = JSON.parse(ress);
                                            status = res['status'];
                                            message = res['message'];
                                            console.log(ress);
                                            switch (status) {
                                                case '1':
                                                    showNotification('Congratulations', 'Your top up was successful. Your balance is now: $' + res["balance"] + ' . You can proceed to log in.')
                                                    break;
                                                case '0':
                                                    showNotification('Sorry', message);
                                                    break;
                                                default:
                                                    showNotification('Sorry', 'An unknown error occured');
                                                    break;
                                            }

                                        },
                                        error: function (jqXHR, exception) {
                                            $('#top-up-submit-button').text('Submit');
                                            $('#top-up-submit-button').removeAttr('disabled');
                                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to top up account using pin');
                                            console.log(jqXHR);
                                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                                        },
                                        timeout: 10000
                                    });
                                }
                            });
                        </script>


                    </div>

                    <!--Tab 4: TOPUP User / Buy Voucher Finished-->

                    <!--Tab 5: Other Success Menu-->
                    <div class="tab-pane fade" id="tab5success">

                        <form class="login-form" role="form" method="post" action="">

                            <label class="custom-label" for="fname">First Name:</label><br>
                            <input class="auth-code-input" type="text" id="fname" size="35" placeholder="eg: John" autocomplete="off">
                            <p class="help-text green-text">Please enter your first name.</p>



                            <label class="custom-label" for="lname">Last Name:</label>
                            <input class="auth-code-input" type="text" id="lname" size="35" placeholder="eg: Doe" autocomplete="off">
                            <p class="help-text green-text">Please enter your surname.</p>


                            <label class="custom-label" for="email">Email:</label>
                            <input  class="auth-code-input" type="text" id="email" size="35" placeholder="eg: johndoe@gmail.com" autocomplete="off">
                            <p class="help-text green-text">Please enter the email address you want to register with.</p>

                            <label class="custom-label" for="mobilenum">Mobile Number:</label>
                            <input class="auth-code-input" type="text" id="mobilenum" size="35" placeholder="eg: 0772345678" autocomplete="off">
                            <p class="help-text green-text">Please enter a valid phone number</p>

                            <label class="custom-label" for="new_cust_username">Preferred Username:</label>
                            <input class="auth-code-input" type="text" id="reg_username" size="35" placeholder="eg: johndoe" autocomplete="off">
                            <p class="help-text green-text">Please enter a unique username.</p>

                            <button class="custom-button" type="button" id="register-submit-button" value="Submit">Submit</button>

                        </form>
                        <script type="text/javascript">

                            $('#register-submit-button').click(function () {


                                var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                var phone_regex = /^0{1}[1-9]{1}[0-9]{8}$/;
                                var submittable = true;
                                var error_mess = 'Please double check the following fields: ';


                                if ($('#fname').val().toString().length < 1) {
                                    submittable = false;
                                    error_mess = error_mess.concat('Firstname, ');
                                }

                                if ($('#lname').val().toString().length < 1) {
                                    submittable = false;
                                    error_mess = error_mess.concat('Lastname, ');
                                }

                                if (!regex.test($('#email').val())) {
                                    submittable = false;
                                    error_mess = error_mess.concat('Email, ');
                                }

                                if (!phone_regex.test($('#mobilenum').val())) {
                                    submittable = false;
                                    error_mess = error_mess.concat('Mobile Number, ');
                                }

                                if ($('#reg_username').val().toString().length < 1) {
                                    submittable = false;
                                    error_mess = error_mess.concat('Username');
                                }

                                if (submittable) {
                                    $('#register-submit-button').text('Loading...');
                                    $('#register-submit-button').attr('disabled');

                                    $.ajax({//create an ajax request to getUserInfo.php when doc loads
                                        type: "POST",
                                        url: "./ajax-scripts/register.php",
                                        data: {
                                            remote_ip: '<?php echo $remote_ip; ?>',
                                            firstname: $('#fname').val(),
                                            surname: $('#lname').val(),
                                            mobile_num: $('#mobilenum').val(),
                                            email: $('#email').val(),
                                            username: $('#reg_username').val(),
                                        },
                                        dataType: "json", //expect json to be returned  

                                        success: function (response) {
                                            $('#register-submit-button').text('Submit');
                                            $('#register-submit-button').removeAttr('disabled');
                                            ress = JSON.stringify(response);
                                            res = JSON.parse(ress);
                                            status = res['status'];
                                            message = res['message'];
                                            console.log(ress);
                                            switch (status) {
                                                case '1':
                                                    showNotification('Congratulations', 'Registration success! All you have to do now is enter the authorisation code sent to your mobile number in the following page, in order to receiver your log in details. Please wait a few seconds while we redirect you.');
                                                    setTimeout(redir, 10000);
                                                    function redir() {
                                                        window.location = './authorisation.php';
                                                    }
                                                    break;
                                                case '0':
                                                    showNotification('Sorry', message);
                                                    break;
                                                default:
                                                    showNotification('Sorry', 'An unknown error occured');
                                                    break;
                                            }

                                        },
                                        error: function (jqXHR, exception) {
                                            $('#register-submit-button').text('Submit');
                                            $('#register-submit-button').removeAttr('disabled');
                                            console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to register new user');
                                            console.log(jqXHR);
                                            showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                                        },
                                        timeout: 10000
                                    });
                                } else {
                                    showNotification('Sorry', error_mess);
                                }
                            });
                        </script>
                    </div>
                    <!--Tab 5: Other Success Menu Finished-->

                    <!--Tab 6: Other Success Menu-->
                    <div class="tab-pane fade" id="tab6success">
                        Success 6
                    </div>
                    <!--Tab 6: Other Success Menu Finished-->

                </div>
            </div>



            <!--Finished Tabbed Menu-->

        </div>

        <?php include './ui_partials/_help_button.php'; ?>

    </div>


    <?php include './ui_partials/_footer.php'; ?>

    <?php include './ui_partials/_notifier.php'; ?>

    <script type="text/javascript">

        // this function should have no alearts since it runs in the backround
        // it updates the users visits
        $(document).ready(function () {
            $.ajax({//create an ajax request to getUserInfo.php when doc loads
                type: "POST",
                url: "./ajax-scripts/updateUserVisits.php",
                data: {mac: '<?php echo $mac; ?>', locationid: '<?php echo $locationid; ?>', locationname: '<?php echo $locationname; ?>'},
                dataType: "json", //expect json to be returned  

                success: function (response) {
                    ress = JSON.stringify(response);
                    res = JSON.parse(ress);
                    status = res['status'];
                    message = res['message'];
                    console.log(ress);
                    switch (status) {
                        case '1':
                            break;
                        case '0':
                            //showNotification('Sorry', message);
                            break;
                        default:
                            //showNotification('Sorry', 'An unknown error occured');
                            break;
                    }

                },
                error: function (jqXHR, exception) {
                    console.log('AJAX responded with error code: ' + jqXHR.status + ' when it tried to update user visits');
                    console.log(jqXHR);
                    //showNotification('Sorry', 'There was an error processing this request. Please try again later.');
                },
                timeout: 10000
            });
        });
    </script>
</body>
</html>

