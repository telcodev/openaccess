<?php
$mac = $_POST['mac'];
$ip = $_POST['ip'];
$username = $_POST['username'];
$password = $_POST['password'];
$linklogin = $_POST['link-login'];
$linkorig = $_POST['link-orig'];
$error = $_POST['error'];
$chapid = $_POST['chap-id'];
$chapchallenge = $_POST['chap-challenge'];
$linkloginonly = $_POST['link-login-only'];
$linkorigesc = $_POST['link-orig-esc'];
$macesc = $_POST['mac-esc'];
$remote_ip = $_POST['remote_ip'];

if (!isset($linklogin) || $linklogin == '') { 
    echo '<h2>Hotspot initialisation error!</h2>'
    . '<br>'
    . '<h4>There was an error when we tried to collect details about your device. We recommend the following steps in order to get browsing again. </h4>'
    . '<br>'
    . '<ul>'
    . '<li>Go to your device\'s WiFi settings</li>'
    . '<li>Switch off your device\'s WiFi connectivity, wait a few seconds then switch your WiFi back on.</li>'
    . '<li>Connect to the OpenAccess Wireless network.</li>'
    . '<li>If this captive portal does not show automatically, open your web browser and visit any site to bring up the captive portal</li>'
    . '</ul>'
    . '<br>'
    . '<h5>For security reason, we will not proceed to log you in.</h5>';
    $linklogin = 'http://openaccess.co.zw/alogin';
    exit();
}


if (isset($_POST['mac'])) {
    $mac = $_POST['mac'];
} else {
    echo '<h2>Hotspot initialisation error!</h2><br>';
    echo '<h4>There was an error when we tried to collect details about your device. For security reasons, you will have to restart the registration process</h4> <br>';
    echo '<h5>This page will redirect you in a short while</h5>';
    header('refresh:10; url=' . $linklogin);
    echo '<p>If the page does not redirect you automatically, <a href="' . $linklogin . '">click here</a> </p>';

    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            OpenAccess Main Login Form
        </title>
        <?php include_once("./inc/analyticstracking.php"); ?>
    </head>
    <body>
        <!--The user failed to mac authenticate try again so we can get the radius response from flogin -->
        <form id="sendin" name="sendin" action="<?php echo $linklogin; ?>" method="post">
            <input type="hidden" name="mac" id="mac" value="<?php echo $mac; ?>" />
            <input type="hidden" name="username" id="username" value="<?php echo $username; ?>" />
            <input type="hidden" name="password" id="password" value="<?php echo $password; ?>" />
            <input type="hidden" name="ip" id="ip" value="<?php echo $ip; ?>" />
            <input type="hidden" name="link-login" id="link-login" value="<?php echo $linklogin; ?>" />
            <input type="hidden" name="link-orig" id="link-orig" value="<?php echo $linkorig; ?>" />
            <input type="hidden" name="error" id="error" value="<?php echo $error; ?>" />
            <input type="hidden" name="chap-id" id="chap-id" value="<?php echo $chapid; ?>" />
            <input type="hidden" name="chap-challenge" id="chap-challenge" value="<?php echo $chapchallenge; ?>" />
            <input type="hidden" name="link-login-only" id="link-login-only" value="<?php echo $linkloginonly; ?>" />
            <input type="hidden" name="link-orig-esc" id="link-orig-esc" value="<?php echo $linkorigesc; ?>" />
            <input type="hidden" name="mac-esc" id="mac-esc" value="<?php echo $macesc; ?>" />
            <input type="hidden" name="remote_ip" id="remote_ip" value="<?php echo $remote_ip; ?>" />
        </form> 
        <script type="text/javascript"> 
            document.getElementById("sendin").submit();
        </script>

        <br /><div style="color: #FF8080; font-size: 9px"><?php echo $error; ?></div>

    </body>
</html>
