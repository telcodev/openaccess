<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
        <title>Welcome to OpenAccess WiFi</title>
        <link rel="icon" type="image/png" href="./img/favicon.ico">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./bootstrap/js/jquery-3.1.1.min.js"></script>
        <script src="./bootstrap/js/bootstrap.min.js"></script>
        <?php include_once("./inc/analyticstracking.php"); ?>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2751462220169204",
                enable_page_level_ads: true
            });
        </script> 
    </head>

    <body class="body">

        <?php include './ui_partials/_header.php'; ?>

        <?php include './ui_partials/_sidebar.php'; ?>

        <!-- Header -->
        <div class="main_body remove-flex">

            <div class="form">

                <noscript>
                <a href='./advertising/www/delivery/ck.php?n=ae75f5aa&amp;cb=201603171138' target='_blank'>
                    <img src='./advertising/www/delivery/avw.php?zoneid=23&amp;cb=201603171138&amp;n=ae75f5aa' class="img-responsive" border='0' />
                </a>
                </noscript> 

                <p class="custom-welcome-text">Welcome</p>
                <p class="help-text">Keep this page open while you browse</p>

                <iframe src="http://openaccess.co.zw/status" height="250" frameborder="0"></iframe>

                <p class="help-text">You can <a href="http://openaccess.co.zw/logout" >logout of your session!</a><br>
                    Or reset your password <a target="_blank" href="./recover_details.php">Here</a>.</p>

            </div>
            <!-- /.container -->

            <?php include './ui_partials/_help_button.php'; ?>

        </div>


        <?php include './ui_partials/_footer.php'; ?>

        <?php include './ui_partials/_notifier.php'; ?>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-66375359-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </body>

</html>
